function requestListener () {
	generateGraph(this.responseText);
	populateLists(this.responseText);
}

function populateLists(jsonRawData) {
	console.log("populating buttons");
	data = JSON.parse(jsonRawData);

	selectList1 = document.getElementById("number_1");
	selectList2 = document.getElementById("number_2");
	selectList3 = document.getElementById("links");

	for (var i = 0; i < data.length; i++) {
		for (var j = 0; j < data[i].payloads.length; j++) {
			selectList1.options[selectList1.options.length] = new Option(data[i].payloads[j] + "\t(Node " + data[i].name + ")" , data[i].payloads[j]);
			selectList2.options[selectList2.options.length] = new Option(data[i].payloads[j] + "\t(Node " + data[i].name + ")" , data[i].payloads[j]);
		}
	}

	for (var i = 4; i < data.length / 2; i++) {
		selectList3.options[selectList3.options.length] = new Option(i, i);
	}
}

function onResetButtonClick() {
	resetNetwork();
}

function onCheckButtonClick() {
	resetNetwork();

	var selectList1 = document.getElementById("number_1");
	var selectList2 = document.getElementById("number_2");
	var selectList3 = document.getElementById("links");

	var num1 = selectList1.options[selectList1.selectedIndex].value;
	var num2 = selectList2.options[selectList2.selectedIndex].value;
	var limit = selectList3.options[selectList3.selectedIndex].value;

	data = {
		"number_1": num1,
		"number_2": num2,
		"limit": limit
	};

	var xmlHttp = new XMLHttpRequest();   // new HttpRequest instance 
	xmlHttp.open("POST", "check", true);
	xmlHttp.setRequestHeader("Content-Type", "application/json");

	xmlHttp.onreadystatechange = function() {
		console.log(this.response);
		var obj = JSON.parse(this.response);
		var resultText = document.getElementById("result_text");

		if (obj["status"] == true) {
			showPath(obj, "minimumPathSuccessNode");
			resultText.innerHTML = "[SUCCESS] Calls between " + obj["source_number"] + " & " + obj["destination_number"] + " CAN be made! Distance between these two numbers is " + obj["distance"] + ".";
		}

		else if(obj["status"] == false && obj["not_connected"]) {
			resultText.innerHTML = "[ERROR] Calls between " + obj["source_number"] + " & " + obj["destination_number"] + " CANNOT be made! They are not connected!";
		}

		else {
			showPath(obj, "minimumPathFailNode");
			resultText.innerHTML = "[ERROR] Calls between " + obj["source_number"] + " & " + obj["destination_number"] + " CANNOT be made! Distance between these two numbers is " + obj["distance"] + " while limit is " + obj["limit"] + ".";
		}
	};

	xmlHttp.send(JSON.stringify(data));
}

function handleNotConnected() {
	console.log("Not connected!");
}
function resetNetwork(data) {
	for (var i = 0; i < nodes.length; i++) {
		nodes.update([{id: nodes._data[i]["id"], group: "defaultNode"}]);
	}

	setGroupAttributes("defaultNode", false)

	var resultText = document.getElementById("result_text");
	resultText.innerHTML = "";
}

function setGroupAttributes(groupName, hidden) {
	for (var i = 0; i < nodes.length; i++) {
		if (nodes._data[i]["group"] === groupName) {
			nodes.update([{id: nodes._data[i]["id"], hidden: hidden}]);
		}
	}
}

function showPath(data, group) {
	var lastNode = data["destination"]

	nodes.update([{id: lastNode, group: group}]);
	console.log(edges);

	while (data["prev"][lastNode.toString()] != null) {
		edgeId = getEdgeId(lastNode, data["prev"][lastNode.toString()]);
		console.log(edgeId);

		edges.update([{id: edgeId}])
		lastNode = data["prev"][lastNode.toString()];
		nodes.update([{id: lastNode, group: group}]);
	}

	setGroupAttributes("defaultNode", true);
}

function getEdgeId(source, destination) {
	edge = edges.get(source + "_" + destination);

	if (edge != undefined)
		return edge.id

	edge = edges.get(destination + "_" + source);

	if (edge != undefined)
		return edge.id

	return null
}

function generateGraph(jsonRawData) {
	data = JSON.parse(jsonRawData);
	
	var container = document.getElementById("graph_network");
	nodes = new vis.DataSet();
	edges = new vis.DataSet();


	var options = {
		groups: {
			defaultNode: {color:{background:"#86d5ff", border: "#257fc4"}, borderWidth:3},
			minimumPathSuccessNode: {color:{background: "#91ff43", border: "#5cad2d"}, borderWidth:3},
			minimumPathFailNode: {color:{background: "#ff874f", border: "#bd4402"}, borderWidth:3}
		},

		interaction: {
			zoomView: false,
			dragNodes:true,
    		dragView: false,
		},

		physics: {
			enabled: false
		}
	};

	// Create nodes
	for (var i = 0; i < data.length; i++) {
		var prefix = data[i].payloads[0].substring(0,3);
		var payloadString = ""

		for (var j = 0; j < data[i].payloads.length; j++) {
			payloadString += (data[i].payloads[j] + "<br>")
		}

		var node = {
			id: data[i].name, 
			label: "Node " + data[i].name + "\n(" + prefix + ")",
			shape: "circle",
			group: "defaultNode",
			chosen: false,
			title: payloadString
		};
		
		nodes.add(node);
	}

	// Create edges
	for (var i = 0; i < data.length; i++) {
		for (var j = 0; j < data[i].neighbors.length; j++) {
			edge = {
				id: data[i].name + "_" + data[i].neighbors[j],
				from: data[i].name,
				to: data[i].neighbors[j],
				chosen: false,
				color: {
					inherit: "from"
				}
			};

			if (edges.get(data[i].neighbors[j] + "_" + data[i].name) == null) {
				edges.add(edge);
			}
		}
	}

	var visData = {
		nodes: nodes,
		edges: edges
	}

	network = new vis.Network(container, visData, options);
	
}

var xmlHttp = new XMLHttpRequest();
xmlHttp.addEventListener("load", requestListener);
xmlHttp.open("GET", "graph");
xmlHttp.send();

console.log("end")