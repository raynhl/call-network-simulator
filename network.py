import argparse
import random
import sys
import json
import math
import os
import urllib
from flask import Flask, render_template, jsonify, request


app = Flask(__name__)

@app.route("/")
def mainpage():
	return render_template("mainpage.html")

@app.route("/graph")
def handle_get_graph():
	return jsonify(graph.network_file_data)

@app.route("/check", methods=["POST"])
def handle_check():
	global graph

	if request.method == "POST":
		data = request.get_json(silent=True)
		payload = check_numbers(data)

	return jsonify(payload)


class NoSuchNumberException(Exception):
	def __init__(self, message):
		self.message = message

class PhoneNumber:
	def __init__(self, prefix):
		number_generator = NumberGenerator()
		self.prefix = prefix
		self.number = number_generator.new_number(self.prefix)

	# Overriding the __str__ function to print number
	def __str__(self):
		return self.number

	def get_prefix(self):
		return self.prefix

	def get_number(self):
		return self.number

class NumberGenerator:
	def __init__(self):
		pass

	def new_number(self, prefix):
		number = str(prefix)

		for i in range(0,7):
			number += str(random.randint(0,9))

		return number

	def new_prefix(self):
		prefix = ""

		for i in range(0,3):
			prefix += str(random.randint(0,9))

		return prefix

class Node:
	def __init__(self, name, payloads = None):
		self.name = name
		self.payloads = payloads
		self.adjacent = {}

	def get_name(self):
		return self.name

	def add_neighbor(self, neighbor, weight = 1):
		self.adjacent[neighbor] = weight

	def get_neighbors(self):
		return [(node.get_name(), self.adjacent[node])  for node in self.adjacent.keys()]

	def get_number_of_neighbors(self):
		return len(self.adjacent)

	def get_payloads(self):
		return self.payloads

	def have_number(self, number):
		return (number in self.payloads)

class Graph:
	def __init__(self, network_file):
		self.number_of_nodes = 0
		self.number_of_edges = 0
		self.nodes = {}
		self.shortest_paths = {}
		self.network_file = network_file

		print("reading from {}".format(self.network_file))
		with urllib.request.urlopen(self.network_file) as json_raw_data:
			self.network_file_data = json.load(json_raw_data)

		self.initialize_graph(self.network_file)
		# self.print_graph()

	def initialize_graph(self, network_file):
		with urllib.request.urlopen(network_file) as json_raw_data:
			data = json.load(json_raw_data)
		
		if not data:
			print("[FATAL] Something is wrong with the network file. Program will now exit.")
			sys.exit(1)

		# Populating vertices
		for node in data:
			node_obj = Node(node["name"], payloads = node["payloads"])
			self.add_node(node_obj)

		# Adding edges
		for node in data:
			for neighbor in node["neighbors"]:
				self.add_edge(node["name"], neighbor, cost = 1)

		print("\n[SUCCESS] Graph initialized.\n")


	# Overriding iterator for graph object
	def __iter__(self):
		return iter(self.nodes.values())

	def get_number_of_nodes(self):
		return self.number_of_nodes

	def get_number_of_edges(self):
		return self.number_of_edges

	def add_node(self, node):
		self.number_of_nodes += 1
		self.nodes[node.get_name()] = node
		return node

	def add_edge(self, source, destination, cost = 1):
		if source not in self.nodes:
			source_node = Node(source)
			self.add_node(source_node)

		if destination not in self.nodes:
			destination_node = Node(destination)
			self.add_node(destination_node)

		self.nodes[source].add_neighbor(self.nodes[destination], cost)
		self.nodes[destination].add_neighbor(self.nodes[source], cost)

	def get_nodes(self):
		return self.nodes.keys()

	def get_node(self, name):
		return self.nodes[name]

	def get_affliated_node(self, number):
		for node_key in self.get_nodes():
			if str(number) in self.nodes[node_key].get_payloads():
				return node_key

		return None

	def print_nodes(self):
		print("[INFO] Total number of nodes: {}".format(self.number_of_nodes))
		for node in self.nodes:
			print(node)

	def print_graph(self):
		for node in self:
			print("Node {} ------- {} neighbor(s).".format(node.get_name(), node.get_number_of_neighbors()))
			
			for neighbor in node.get_neighbors():
				print("|---> {}".format(neighbor))

			print("===============================")

	def can_be_connected(self, numbers, limit):
		node_0 = self.get_affliated_node(numbers[0])

		if node_0 is None:
			raise NoSuchNumberException("[FATAL ERROR] {} does not exist on any nodes!".format(numbers[0]))

		node_1 = self.get_affliated_node(numbers[1])
		if node_1 is None:
			raise NoSuchNumberException("[FATAL ERROR] {} does not exist on any nodes!".format(numbers[1]))

		distance, prev = self.get_shortest_path(node_0, node_1)

		if (distance == math.inf) or (distance > limit):
			return False, distance, prev

		return True, distance, prev

	def get_shortest_path(self, source_node, destination_node):
		dist, prev = self.bfs(source_node, destination_node)		
		prev.reverse()

		path = {}
		for index, node in enumerate(prev):
			if index == len(prev) - 1:
				path[node] = None
				break

			path[node] = prev[index + 1]

		return dist, path

	def dijkstra(self, source_node):
		vertices = []
		dist = {}
		prev = {}

		for node in self.get_nodes():
			dist[node] = math.inf
			prev[node] = None
			vertices.append(node)

		dist[source_node] = 0

		while len(vertices) != 0:
			sorted_dist = sorted(dist, key = dist.get)		
			minimum_node = 0

			for index, value in enumerate(sorted_dist):
				if value in vertices:
					minimum_node = value
					break

			vertices.remove(minimum_node)

			for neighbor in self.get_node(minimum_node).get_neighbors():
				# neighbor[0] is node number, neighbor[1] is distance
				alt_dist = dist[minimum_node] + neighbor[1]
				if (alt_dist < dist[neighbor[0]]):
					dist[neighbor[0]] = alt_dist
					prev[neighbor[0]] = minimum_node

		return dist, prev

	def bfs(self, source_node, destination_node):
		visited_nodes = {}
		path = {}
		queue = []
		
		for i in range(self.get_number_of_nodes()):
			visited_nodes[i] = False

		queue.append([source_node])
		visited_nodes[source_node] = True

		while queue:
			path = queue.pop(0)
			current_node = path[-1]

			if current_node == destination_node:
				return len(path) - 1, path

			current_node_obj = self.get_node(current_node)
			
			for neighbor in current_node_obj.get_neighbors():
				if visited_nodes[neighbor[0]] == False:
					alternate_path = list(path)
					alternate_path.append(neighbor[0])
					queue.append(alternate_path)
					visited_nodes[neighbor[0]] = True


def generate_network(number_of_nodes, max_number_per_node):
	number_generator = NumberGenerator()

	data_list = []
	for node_index in range(number_of_nodes):
		node_data = {}
		node_data["name"] = node_index
		node_data["payloads"] = []
		node_data["neighbors"] = []

		# Prepare to randomly generate edges
		remaining_nodes = list(range(0, number_of_nodes))
		remaining_nodes.remove(node_index)

		if len(remaining_nodes) > 10:
			number_of_connected_nodes = random.randint(1, int(len(remaining_nodes)/4))

		else:
			number_of_connected_nodes = random.randint(1, len(remaining_nodes))

		random_node_indices = []
		count = 0

		while(count < number_of_connected_nodes):
			random_node = random.choice(remaining_nodes)

			if random_node not in node_data["neighbors"]:
				node_data["neighbors"].append(random_node)
				count += 1
		# Generating edges - END

		# Prepare to generate local phone numbers
		prefix = number_generator.new_prefix()

		for number in range(random.randint(1, max_number_per_node)):
			phone_number = PhoneNumber(prefix)
			node_data["payloads"].append(phone_number.get_number())

		data_list.append(node_data)
		# Generating local phone number - END

	with open("network.json", "w") as output_file:
		json.dump(data_list, output_file,  indent = 4, sort_keys=True)

	print("[SUCCESS] Network generated")

def check_numbers(data):
	global graph
	
	numbers = []
	numbers.append(data["number_1"])
	numbers.append(data["number_2"])

	limit = int(data["limit"])

	try:
		status, distance, prev = graph.can_be_connected(numbers, limit)
		
		payload = {
			"status": status,
			"distance": distance,
			"prev": prev,
			"limit": limit,
			"destination": graph.get_affliated_node(numbers[1]),
			"source_number": data["number_1"],
			"destination_number": data["number_2"],
			"not_connected": False
			}

		if distance == math.inf:
			payload["not_connected"] = True

	except NoSuchNumberException as err:
		print(err)

	return payload

graph = Graph("https://gitlab.com/raynhl/call-network-simulator/raw/master/network.json")

if __name__ == "__main__":
	parser = argparse.ArgumentParser("A call network simulator")
	
	subparsers = parser.add_subparsers()

	parser_a = subparsers.add_parser("netgen", help = "Generate a new network")
	parser_a.add_argument("-n", help = "Number of nodes / vertex", required = True)
	parser_a.add_argument("-m", help = "Maximum number of phone numbers per node", type = int, default = 50)
	parser_a.set_defaults(which='netgen')
	
	try:
		args = vars(parser.parse_args())

	except:
		parser.print_help()
		sys.exit(1)

	try:
		if args["which"] == "netgen":
			generate_network(int(args["n"]), int(args["m"]))
			sys.exit(0)

	except:
		app.run()
		
